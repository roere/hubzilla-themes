<?php

/**
 *   * Name: Pirate-Ironside
 *   * Description: Pirate party standard theme
 *   * Version: 1.0
 *   * MinVersion: 8.9
 *   * MaxVersion: 10.0
 *   * Author: Fabrixxm
 *   * Maintainer: Mike Macgirvin
 *   * Maintainer: Mario Vavti
 *   * Theme_Color: rgb(248, 249, 250)
 *   * Background_Color: rgb(254,254,254)
 */


function pirate-ironside_init() {

}
