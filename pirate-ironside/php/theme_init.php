<?php
require_once('view/php/theme_init.php');

head_add_css('/library/fork-awesome/css/fork-awesome.min.css');
head_add_css('/library/bootstrap-tagsinput/bootstrap-tagsinput.css');
head_add_css('/library/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css');

head_add_js('/vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js');
head_add_js('/library/bootbox/bootbox.min.js');
head_add_js('/library/bootstrap-tagsinput/bootstrap-tagsinput.js');
head_add_js('/library/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js');

$pirate-ironside_mode = '';
$pirate-ironside_navbar_mode = '';

if (local_channel()) {
	$pirate-ironside_mode = ((get_pconfig(local_channel(), 'pirate-ironside', 'dark_mode')) ? 'dark' : 'light');
	$pirate-ironside_navbar_mode = ((get_pconfig(local_channel(), 'pirate-ironside', 'navbar_dark_mode')) ? 'dark' : 'light');
}

if (App::$profile_uid) {
	$pirate-ironside_mode = ((get_pconfig(App::$profile_uid, 'pirate-ironside', 'dark_mode')) ? 'dark' : 'light');
	$pirate-ironside_navbar_mode = ((get_pconfig(App::$profile_uid, 'pirate-ironside', 'navbar_dark_mode')) ? 'dark' : 'light');
}

if (!$pirate-ironside_mode) {
	$pirate-ironside_mode = ((get_config('pirate-ironside', 'dark_mode')) ? 'dark' : 'light');
	$pirate-ironside_navbar_mode = ((get_config('pirate-ironside', 'navbar_dark_mode')) ? 'dark' : 'light');
}

App::$page['color_mode'] = 'data-bs-theme="' . $pirate-ironside_mode . '"';
App::$page['navbar_color_mode'] = (($pirate-ironside_navbar_mode === 'dark') ? 'data-bs-theme="' . $pirate-ironside_navbar_mode . '"' : '');
